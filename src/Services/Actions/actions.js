import {
        ADD_TO_CART,
        REMOVE_FROM_CART,
        FILTER_BY_SIZE,
        CART_STATUS,
        REDUCE_QUANTITY,
        CLEAR_CART,
        FILTER_BY_VALUE
    } from "../constants"

export const addTocart = (data)=>{
    return{
        type:ADD_TO_CART,
        data:data
    }
}

export const removeFromCart = (data)=>{
    return{
        type:REMOVE_FROM_CART,
        data:data
    }
}

export const reduceQuantity = (data)=>{
    return{
        type:REDUCE_QUANTITY,
        data:data
    }
}

export const cartItemsStatus = ()=>{
    return{
        type:CART_STATUS,
        status:Boolean
    }
}

export const clearCart = ()=>{
    return{
        type:CLEAR_CART,
        data:[]
    }
}

export const filterBySize = (data)=>{
    return{
        type:FILTER_BY_SIZE,
        data:data
    }
}

export const filterBySearchValue = (data)=>{
    return{
        type:FILTER_BY_VALUE,
        data:data
    }
}