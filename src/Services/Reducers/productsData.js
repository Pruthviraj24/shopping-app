import productsData from "../../data/data";
import { FILTER_BY_SIZE,FILTER_BY_VALUE } from "../constants";


const initialState = {
    productsData,
    tempData:[],
    sizeS:false,
    sizeXS:false,
    sizeXL:false,
    sizeM:false,
    sizeL:false,
    sizeXXL:false
}



const products = (state = initialState,action)=>{
    

    switch(action.type){
        case FILTER_BY_SIZE:

            if(action.data.size === "CLEAR"){
                return {
                    ...state,
                    productsData:productsData,
                    tempData:[],
                    sizeS:false,
                    sizeXS:false,
                    sizeXL:false,
                    sizeM:false,
                    sizeL:false,
                    sizeXXL:false
                }
            }else{
                // console.log(action.data.updatedData);
                return {
                    ...state,
                    productsData:action.data.updatedData,
                    sizeXS:action.data.size === "XS" ? !state.sizeXS : state.sizeXS,
                    sizeS:action.data.size === "S" ? !state.sizeS : state.sizeS,
                    sizeM:action.data.size === "M" ? !state.sizeM : state.sizeM,
                    sizeL:action.data.size === "L" ? !state.sizeL : state.sizeL,
                    sizeXL:action.data.size === "XL" ? !state.sizeXL : state.sizeXL,
                    sizeXXL:action.data.size === "XXL" ? !state.sizeXXL : state.sizeXXL
                }
            }
        case FILTER_BY_VALUE:
    
            const products = productsData.filter(eachProduct => eachProduct.title.toLowerCase().includes(action.data))
            return{
                ...state,
                productsData:products
            }
        default:
            return state 
    }
}



export default products