import { ADD_TO_CART,REMOVE_FROM_CART,CART_STATUS,REDUCE_QUANTITY,CLEAR_CART} from "../constants"

const initialState = {
    cartData:[],
    status:false
}

const cartItemsStatus = (state=initialState,action)=>{

    switch(action.type){
        case ADD_TO_CART:
            const itemCheck = state.cartData.some(eachItem => eachItem.id ===  action.data.id)
            if(itemCheck){
                const updatedCart = state.cartData.map((eachItem)=>{
                    if(eachItem.id === action.data.id){
                        eachItem.quantity ++
                    }
                    return eachItem
                })
                return {
                    ...state,
                    cartData:updatedCart
            }
            }else{
                return {
                    ...state,
                    cartData:[...state.cartData,{...action.data}]
                }
            }
        case REMOVE_FROM_CART:
            const updatedData  = state.cartData.filter(eachItem=>eachItem.id !== action.data.id)
            return {
                ...state,
                cartData:updatedData
            }
        case REDUCE_QUANTITY:
           
            if(action.data.quantity === 1){
                const updatedData  = state.cartData.filter(eachItem=>eachItem.id !== action.data.id)
                return {
                    ...state,
                    cartData:updatedData
                }
            }else{
                const updatedCartData = state.cartData.filter(eachItem=>{
                    if(eachItem.id === action.data.id){
                        eachItem.quantity-=1
                        return eachItem
                    }else{
                        return eachItem
                    }
                })
                return {
                    ...state,
                    cartData:updatedCartData
                }
            }
        case CART_STATUS:
            return {
                ...state,
                status:!state.status
            }
        case CLEAR_CART:
            return {
                ...state,
                cartData:[]
            }
        default:
            return state 
    }
}



export default cartItemsStatus