import cartItemsStatus from "./cartItems"
import products from "./productsData"
import {combineReducers} from "redux"

export default combineReducers({
    cartItemsStatus,
    products,
})

