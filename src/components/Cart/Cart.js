import {Component} from "react"
import CartItem from "../CartItem/CartItem"
import {BsCart4} from "react-icons/bs"
import {GiTireIronCross} from "react-icons/gi"
import {FaRupeeSign} from "react-icons/fa"
import {v4 as uuid} from "uuid"
import "./index.css"
import { connect } from "react-redux";
import {cartItemsStatus,addTocart,clearCart} from "../../Services/Actions/actions"

const Server_Url = "http://192.168.7.114:3000"
// const Server_Url = "http://localhost:3000"


class Cart extends Component{
    state = {email:"", data:[],errorMsg:false}
    


    validateEmailId = (email)=>{
        return /\S+@\S+\.\S+/.test(email);
    }


    checkOut = async (event) =>{
        event.preventDefault()
        const {clearCartHandler,cartData} = this.props
        const {email} = this.state

        if(this.validateEmailId(email)){
           
            const orderData = {
                orderId:uuid(),
                userEmail:email,
                orderDetails:cartData,
            }
            console.log(orderData);
        
        if(cartData.length > 0){
            const url = `${Server_Url}/postdata`
            const options = {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                  },
                body: JSON.stringify(orderData),
              }
    
            const response = await fetch(url,options)
            const responseData = await response.json()
    
            console.log(responseData);

            const {cartStatusHandler} = this.props
            cartStatusHandler()
            clearCartHandler()
            this.setState({errorMsg:false})
        }else{
            alert("Please add Items to cart before checkout.!")
        }
        }else{
            this.setState({errorMsg:true})
        }
    }



    render(){
        const {cartStatusHandler,status,cartData} = this.props
        const {errorMsg} = this.state
        const totalQuantity = cartData.reduce((acc,eachItem) => {return acc + eachItem.quantity },0)
        const subtotal = cartData.length > 0 ? cartData.reduce((acc,item)=> {return acc+(item.price*item.quantity)},0).toFixed(2) : "0.00"

    
        return(
            <div className="cart-section">
               {status && 
               <div className="cart-list-container">
                    <div className="cart-top-container">
                        <button onClick={cartStatusHandler}  className={`close-button bg-[#242323] hover:animate-spin`} type="button">
                            <GiTireIronCross />
                        </button>
                        <div className="cart-icon-container">
                            <BsCart4 />
                            <sub className="cart-item-count-in-cart-container rounded-full">{totalQuantity}</sub>
                            <p>Cart</p>
                        </div>
                    </div>
                        <ul className="cart-item-container" style={{padding:'0',paddingTop:"115px"}}>
                            {
                                cartData.map((item)=>{
                                return item.quantity > 0 && <CartItem item={item} key={item.id}/>
                                })
                            }
                        </ul>
                    <form onSubmit={this.checkOut} className="checkout">
                        <input className="email-input" onChange={(event) => this.setState({email:event.target.value})} placeholder="Enter Your Email..." type="text" />
                        {errorMsg && <small style={{color:"red",marginLeft:"14px"}}>Please Enter the valid Email*</small>}
                        <div className="checkout-section">
                            <h2>SUBTOTAL</h2>
                            <h2 className="subtotal-amount"><FaRupeeSign style={{marginRight:"4px",fontSize:"1rem"}}/>{` ${subtotal}`}</h2>
                        </div>
                    <button type="submit" className="checkout-button bg-[#2b2a2a] hover:animate-bounce">CHECKOUT</button>
                </form>
                </div>}
            </div>
        )
    }


}

const mapStateToProps = state =>({
    cartData:state.cartItemsStatus.cartData,
    status : state.cartItemsStatus.status
})

const mapDispatchToProps = dispatch =>({
    cartStatusHandler:()=> dispatch(cartItemsStatus()),
    addTocartHandler:()=> dispatch(addTocart()),
    clearCartHandler:()=> dispatch(clearCart())
})


export default connect(mapStateToProps,mapDispatchToProps)(Cart)
