import {Component} from "react"
import { connect } from "react-redux";

import "./index.css"


class Filters extends Component{
    state = {
        products: this.props.productsData,
        filters: new Set(),
      }

    updateProductsBySize = (event)=>{
      const {filterBySizeHandler} = this.props
      filterBySizeHandler(event)
    }


    

    render(){
      const {sizeArray} = this.props

    

        return(
            <div className="bg-filter-container">
                <h4 style={{fontSize:"1rem", fontWeight:"bold"}}>Filters</h4>
                <div className="buttons-container">

                    {sizeArray.map((eachSize,index)=>
                    <div  key={index}>
                      <button className={eachSize.activeSize ? "filter-buttons-active" : "filter-buttons" }  value={eachSize.size} onClick={this.updateProductsBySize} type="button">{ !eachSize.activeSize && ">  "}{eachSize.name}</button>
                    </div>
                    )}
                    
                    <button className="clear-button" value="CLEAR" onClick={this.updateProductsBySize} type="button"> { "> "} Clear</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state =>({
    // productsData:state.products.productsData,
    // sizeS:state.products.sizeS,
    // sizeXS:state.products.sizeXS,
    // sizeXL:state.products.sizeXL,
    // sizeM:state.products.sizeM,
    // sizeL:state.products.sizeL,
    // sizeXXL:state.products.sizeXXL
})

const mapDispatchToProps = dispatch =>({
    // filterBySizeHandler:(data)=> dispatch(filterBySize(data))
})



export default connect(mapStateToProps,mapDispatchToProps)(Filters)