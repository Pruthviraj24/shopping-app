import {Component} from "react"
import Product from "../Products/Products"
import Filters from "../Filters/Filters"
import Cart from "../Cart/Cart"
import { connect } from "react-redux";
import {filterBySize,addTocart,filterBySearchValue} from "../../Services/Actions/actions"
import  {CirclesWithBar} from "react-loader-spinner"
import "./index.css"
import {sizes} from "./../../data/data"
import NavBar from "../Navbar";


// const Server_Url = "http://192.168.7.114:3000"
const Server_Url = "http://localhost:3000"

const apiStatusConstants = {
    initial: "INITIAL",
    success: "SUCCESS",
    failure: "FAILURE",
    inProgress: "IN_PROGRESS",
  };


class Home extends Component{
    state={searchValue:"",
    products:[],
    sizeArray:[...sizes],
    apiStatus:apiStatusConstants.initial,
    moreProducts:[],
    moreProductsapi:apiStatusConstants.initial}
    
    componentDidMount(){
        this.getProductsData()
    }

    getProductsData = async ()=>{
        const url = Server_Url

        const response = await fetch(url)
        const responseData = await response.json()
        console.log(responseData);
        this.setState({
            products:responseData,
            apiStatus:apiStatusConstants.success,
        })

    }

    updateAddItem = ()=>{
        const {filterBySizeHandler} = this.props
        filterBySizeHandler()
    }

    updateItemsData = async (event)=>{
        
        const url = `${Server_Url}/search/:${event.target.value}`

        this.setState({
            apiStatus:apiStatusConstants.inProgress,
            searchValue:event.target.value
        })

        const response =await fetch(url)
        const responseData =await response.json()
        console.log(responseData);

        this.setState({
            products:responseData,
            apiStatus:apiStatusConstants.success,
            moreProducts:[]
        })
    }

    filterBySizeHandler = async (event)=>{

        if(event.target.value !== "CLEAR"){
            const url = `${Server_Url}/size/:${event.target.value}`
            const response =await fetch(url)
            const responseData =await response.json()
            
            this.setState((prevState)=>({
                products:responseData,
                sizeArray:prevState.sizeArray.map(eachSize => {
                    if(eachSize.size === event.target.value){
                      eachSize.activeSize=!eachSize.activeSize
                      
                    }else{
                        eachSize.activeSize=false
                    }
                    return eachSize
                  }),
                moreProducts:[]
            }))
        }else{
            const url = Server_Url
            const response =await fetch(url)
            const responseData =await response.json()
            
            this.setState((prevState)=>({
                products:responseData,
                sizeArray:prevState.sizeArray.map(eachSize => {
                    if(eachSize.activeSize === true){
                      eachSize.activeSize=false
                    }
                    return eachSize
                  })
            }))
        }
 

    }

    getAdditionalData =async ()=>{

        this.setState({
            moreProductsapi:apiStatusConstants.inProgress
        })
        const url = `${Server_Url}/search`
        const response = await fetch(url)
        const responseData = await response.json()
        this.setState(prevState => ({
            products:[...prevState.products,...responseData],
            moreProductsapi:apiStatusConstants.success
        }))
    }


    render(){
        const {addTocartHandler} = this.props
        const {products,sizeArray,apiStatus,moreProducts,searchValue} = this.state
        console.log(products);
    
        return(
            <div >
                <NavBar/>
                <Filters sizeArray={sizeArray}  productsData={products} filterBySizeHandler={this.filterBySizeHandler}/>
                <Cart /> 
                <div className="search-container">
                   <input className="search-input" placeholder="Search for items.." type="search" value={searchValue}  onChange={this.updateItemsData} autoFocus/>
                </div> 
                {apiStatus === apiStatusConstants.success ?
                    <>
                        <ul className="products-container">
                            {
                                products?.map((product)=>{
                                    return <Product product={product} updateAddItem={addTocartHandler}  key={product.id}/>
                                })
                            }
                        </ul>
                            {products.length === 0  && <h2 className="products-container">No Products Found</h2>}
                            {(products.length === 16 && moreProducts.length === 0) &&
                                <button onClick={this.getAdditionalData} type="button" className="load-more-button bg-black mb-50 hover:uppercase">Load More...</button>
                            }
                    </>       :
                    <CirclesWithBar
                            height={60}
                            width={60}
                            radius={5}
                            color="#4fa94d"
                            ariaLabel="ball-triangle-loading"
                            wrapperClass={{}}
                            wrapperStyle={{position:"absolute",top:"50%",right:"46%"}}
                            visible={true}
                        />
                 
                }
        </div>
        )
    }
}

const mapStateToProps = state =>({
    productsData : state.products.productsData
})

const mapDispatchToProps = dispatch =>({
    filterBySizeHandler:data=> dispatch(filterBySize(data)),
    addTocartHandler:data=> dispatch(addTocart(data)),
    filterBySearchValue:data=>dispatch(filterBySearchValue(data))
})



export default connect(mapStateToProps,mapDispatchToProps)(Home)