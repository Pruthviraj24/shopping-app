import {Component} from "react"
import {IoMdAddCircleOutline} from "react-icons/io"
import {AiOutlineMinusCircle} from "react-icons/ai"
import {GiCancel} from "react-icons/gi"
import {FaRupeeSign} from "react-icons/fa"
import { connect } from "react-redux";
import {addTocart,reduceQuantity,removeFromCart} from "../../Services/Actions/actions"
import "./index.css"


class CartItem extends Component{



    addTocartUpdater = ()=>{
        const {addTocartHandler,item} = this.props
        addTocartHandler(item)
    }

    updateRemoveFromCart = ()=>{
        const {removeItem,item} = this.props
        removeItem(item)
    }

    updateReducerQuantity = ()=>{
        const {reduceQuantityHandler,item} = this.props
        reduceQuantityHandler(item)
    }

    
    render(){
        const {item} = this.props
        
        return(
            <li className="cart-item">
            <img className="cartItemImage" src={ require(`./../../static/products/${item.cartImage}`) }  alt=""/>
                <div>
                    <p className="item-title">{item.title}</p>
                    <p className="quantity">{`Quantity: ${item.quantity}`}</p>
                </div>
                <button onClick={this.updateRemoveFromCart} className="delete-item-button bg-black hover:motion-safe:animate-spin" type="button">
                    <GiCancel/>
                </button>
                <p className="item-price"><FaRupeeSign style={{marginRight:"4px",fontSize:"0.8rem"}}/>{` ${(item.price*item.quantity).toFixed(2)}`}</p>
                <button onClick={this.addTocartUpdater} type="button" className="add-more bg-black hover:animate-spin">
                    <IoMdAddCircleOutline />
                </button>
                <button onClick={this.updateReducerQuantity} type="button" className="reduce-item-count bg-black hover:motion-safe:animate-spin">
                    <AiOutlineMinusCircle />
                </button>
        </li>

        )
    }
}

const mapStateToProps = state =>({
    cartData:state.cartItemsStatus.cartData,
})

const mapDispatchToProps = dispatch =>({
    addTocartHandler:(data)=> dispatch(addTocart(data)),
    removeItem:(data)=>dispatch(removeFromCart(data)),
    reduceQuantityHandler:data => dispatch(reduceQuantity(data))   
})


export default connect(mapStateToProps,mapDispatchToProps)(CartItem)
