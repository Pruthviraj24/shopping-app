import { useEffect,useState } from "react"
import UserItems from "../UserItems"
import "./styles.css"


// const Server_Url = "http://192.168.7.114:3000"
const Server_Url = "http://localhost:3000"


const Admin = ()=>{
    const [users, setUsers] = useState([])
    
    const gerUsersData = async ()=>{
        const url = `${Server_Url}/cartdata`
        const response = await fetch(url)
        const responseData = await response.json()
        
        let updatedData = responseData.map(eachData =>{
                return{
                    id:eachData.id,
                    userEmail:eachData.user_email,
                    quantity:eachData.quantity,
                    price:eachData.price,
                    imageUrl:eachData.image_url,
                    title:eachData.title
                }
        })
        updatedData = updatedData.reduce((acc,curr)=>{
            if(acc[curr.userEmail]){
                acc[curr.userEmail]["products"] = [...acc[curr.userEmail].products,{id:curr.id,
                    price:curr.price,
                    quantity:curr.quantity,
                    imageUrl:curr.imageUrl,
                    title:curr.title
                }]
            }else{
                acc[curr.userEmail] = {}
                acc[curr.userEmail]["products"] = [{
                    id:curr.id,
                    price:curr.price,
                    quantity:curr.quantity,
                    imageUrl:curr.imageUrl,
                    title:curr.title
                }]
            }
            
            return acc
        },{})

        setUsers(updatedData)
    }   

    useEffect(()=>{
        gerUsersData()
    },[])

   
    return(
        <div className="admin-container">
            {
                Object.values(users).map((eachUser,index) => {
                        
                        const total = eachUser.products.reduce((acc,curr)=> {return acc + (curr.price * curr.quantity)},0)
                    
                        return(
                        <div key = {Object.keys(users)[index]}>
                            <h3 className="user-email">{Object.keys(users)[index]}</h3>
                            <ul style={{display:"flex",flexWrap:"wrap",alignItems:"center",margin:"40px 0px 40px 50px"}}>
                                {eachUser.products.map(eachItem => <UserItems items={eachItem} key={eachItem.id}/>)}
                            </ul>
                            <h3 style={{textAlign:"start",margin:"-9px 0px 20px 140px",textDecoration:"underline",fontWeight:"bold",fontSize:"1.4rem"}}>SUBTOTAL <small style={{fontWeight:"100"}}>INR {Math.round(total*100)/100}/- </small></h3>
                            <hr />
                        </div>
                        )
                })
            }

        </div>
    )
}

export default Admin