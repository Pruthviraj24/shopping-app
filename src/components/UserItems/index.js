import "./styles.css"
import ReactImageMagnify from 'react-image-magnify';

const UserItems = (props)=>{
    const {items} = props

    const imageSorce =require(`./../../static/products/${items.imageUrl}`) 
    return(
        <div>
            <ReactImageMagnify
                {...{
                  smallImage: {
                    alt: "",
                    isFluidWidth: true,
                    src: imageSorce,
                    width: 400,
                    height: 800
                  },
                  largeImage: {
                    alt: "",
                    src: imageSorce,
                    width: 1000,
                    height: 1800,
                  },
                  enlargedImageContainerDimensions: {
                                  width: '200%',
                                  height: '100%',                            
                  },
                  enlargedImageStyle:{
                    zIndex:0,
                    position:"relative"
                  },
                  enlargedImageContainerStyle:{
                    zIndex:2,
                    position:"absolute"
                  }
                }}
                shouldUsePositiveSpaceLens={true}
                style={{margin:"40px 10px 5px 40px",height:"20px"}}
              />
              <div className="item-details-container">
                <h2 style={{fontWeight:"bold",marginTop:"10px",fontSize:"1.2rem"}}>{items.title}</h2>
                <h4>Price: {` ${String(items.price).split(".")[0]}.`}
                    
                    <span className="price-span">
                    {`${String(items.price).split(".")[1] === undefined ? "00":String(items.price).split(".")[1]}`} INR
                    </span>
                    
                </h4>
                <h4>
                  Quantity: {items.quantity} nos
                </h4>
            </div>
        </div>
    )
}

export default UserItems