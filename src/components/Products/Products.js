import "./index.css"
import { Component } from "react"
import { IoMdAddCircleOutline } from "react-icons/io"
import {AiOutlineMinusCircle} from "react-icons/ai"
import {FaRupeeSign} from "react-icons/fa"
import { connect } from "react-redux";
import {addTocart,reduceQuantity} from "../../Services/Actions/actions"

class Product extends Component{
    state = {cartState:false}
  

    
    addCartItem = ()=>{
        const {updateAddItem,product} = this.props

        this.setState({cartState:true},
           ()=> updateAddItem({...product,quantity:1})
        )
        
    }


    addTocartUpdater = ()=>{
        const {addTocartHandler,product} = this.props
        addTocartHandler(product)
    }


    updateReducerQuantity = ()=>{
        const {reduceQuantityHandler,product} = this.props
        reduceQuantityHandler(product)
    }

    updateProductQuantity = ()=>{
        this.setState({cartState:false})
    }

    render(){
        const {product,cartData} = this.props
        const {productImage,price,title} = product
        
        const quantity = cartData.filter(eachData => eachData.id === product.id)[0]?.quantity
        
    

        return(
            <div className="product-container" style={{padding:"0px"}}> 
                <div>
                    <img className="product-image" src={ require(`./../../static/products/${productImage}`) }  alt={title}/>
                </div>
                <div className="product-details-container">
                <h5 className="product-title">{title}</h5>
                <h4 className="price-header"><FaRupeeSign style={{marginRight:"4px",fontSize:"1rem"}}/>{` ${String(price).split(".")[0]}.`}<span className="price-span">{`${String(price).split(".")[1] === undefined ? "00":String(price).split(".")[1]}`}</span> </h4>
                    {quantity > 0 ?
                    <div className="quantity-button-container">
                        <button className="minus-button" type="button" onClick={this.updateReducerQuantity}>
                            <AiOutlineMinusCircle/>
                        </button>
                        <p className="quantity-element">{quantity}</p>
                        <button className="minus-button" type="button" onClick={this.addCartItem}>
                            <IoMdAddCircleOutline/>
                        </button>
                    </div> 
                : 
                <button onClick={this.addCartItem} className="add-to-cart bg-black mb-50 hover:uppercase" type="button">Add to Cart</button>
                }
                </div>
            </div>
        )
    }
}


const mapStateToProps = state =>({
    cartData:state.cartItemsStatus.cartData,
})

const mapDispatchToProps = dispatch =>({
    addTocartHandler:(data)=> dispatch(addTocart(data)),
    reduceQuantityHandler:data => dispatch(reduceQuantity(data))   
})


export default connect(mapStateToProps,mapDispatchToProps)(Product)
