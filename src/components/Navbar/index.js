import {RiGitlabLine} from "react-icons/ri"
import {BsCart4} from "react-icons/bs"
import { connect } from "react-redux";
import {Link} from "react-router-dom"
import {cartItemsStatus,addTocart,clearCart} from "../../Services/Actions/actions"
import "./styles.css"


const NavBar = (props)=>{
    const {cartData,status,cartStatusHandler} = props
    const totalQuantity = cartData.reduce((acc,eachItem) => {return acc + eachItem.quantity },0)



    return(
        <nav className="nav-bar">
            <button type="button" className="git-lab-button">
                <a href="https://gitlab.com/Pruthviraj24">
                    <RiGitlabLine className="git-labicon  origin-bottom -rotate-12 hover:animate-spin"/>
                </a>
            </button>
            <Link to="/admin">
                <div className="admin-button bg-black mb-50 hover:uppercase">
                    Admin
                </div>
            </Link>
            {!status && <button onClick={cartStatusHandler} type="button" className={`cart-icon bg-[#242323] hover:rotate-45`}>
                <BsCart4/>
                <sub className="cartItem-count rounded-full">{totalQuantity}</sub>
            </button>}
        </nav>
    )
}

const mapStateToProps = state =>({
    cartData:state.cartItemsStatus.cartData,
    status : state.cartItemsStatus.status
})

const mapDispatchToProps = dispatch =>({
    cartStatusHandler:()=> dispatch(cartItemsStatus()),
    addTocartHandler:()=> dispatch(addTocart()),
    clearCartHandler:()=> dispatch(clearCart())
})


export default connect(mapStateToProps,mapDispatchToProps)(NavBar)