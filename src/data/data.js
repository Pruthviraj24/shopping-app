import data from "./data.json"
export const sizes = [
    {
      size:"XS",
      name:"Extra Small",
      activeSize:false
    },
    {
        size:"S",
        name:"Small",
        activeSize:false
      }, 
    {
      size:"M",
      name:"Medium",
      activeSize:false
    },
    {
      size:"L",
      name:"Large",
      activeSize:false},
      {
        size:"XL",
        name:"Extra Large",
        activeSize:false
      },
      {
        size:"XXL",
        name:"Double Extra Large",
        activeSize:false
      }
    ]
const productsData =  data.products
export default productsData