import Home from "./components/Home/Home"
import Admin from "./components/Admin/Admin"
import {BrowserRouter ,Route, Switch} from "react-router-dom"


function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact render={() => (<Home  />)}/>
        <Route path="/admin" exact render={() => (<Admin/>)}/>
      </Switch>
    </BrowserRouter>
  )
}

export default App